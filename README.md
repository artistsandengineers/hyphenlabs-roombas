# Wat

POC Roomba control system built in Node.

# Limitations

Lots. In particular I've not succeeded in reading any sensor data.

# Installing and using

1. Clone repo
2. Install Node.js
3. `cd whatever/hyphenlabs-roombas/MiddleBit`
4. `npm i`
5. `node index.js`
6. Crash!
7. Connect XBee USB dongle
8. Figure out the serial port address (e.g. `/dev/tty.usbetcetc`) 
9. Edit `config_local.json` to set the serial port address.
10. `node index.js`
11. Visit `localhost:3000` in a browser.
12. Click `Enable Full`. This puts the Roomba in a mode where it will listen to us for instructions.  NOTE: All of Roomba's safety sensors are disabled in this mode, so it won't stop when it bumps into walls etc. Be careful
13. Click buttons. They mostly do what you expect.

# If it doesn't work

* Check that the XBee receiver on the Roomba is powered up. There's a toggle switch.
* Check that the XBee receiver on the Roomba is receiving. It'll flash a green light when serial messages are arriving.
* If the Roomba's battery was dead when you received it it might have switched baud rate away from the one that we're using (although I think there's no reason you couldn't use the default, which would probably be sensible). Press and hold the CLEAN button for like 10 seconds until you hear a descending series of notes.

# Fun Facts & Recommended Reading

* Roomba official documentation is [here](https://www.usna.edu/Users/weaprcon/esposito/_files/roomba.matlab/Roomba_SCI.pdf)
* Someone's quite crap (but probably not as a crap as this tbf) implementation for Arduino is [here](https://create.arduino.cc/projecthub/mjrobot/controlling-a-roomba-robot-with-arduino-and-android-device-56970d). There's a lot of opcodes mentioned that don't seem to feature in the offical documentation, including some motor drive modes that might be more useful than the one we're demoing here.
* When you first 'handshake' with the Roomba it'll turn its power light off. This is confusing. If you think you've crashed it lift it off the surface and the wheel sensors will reset it.
* The physical port on the top of the Roomba features some pins that are directly connected to the battery, so watch out for those.

