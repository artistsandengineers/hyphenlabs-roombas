const fs = require('fs');
const SerialPort = require('serialport');
const express = require('express');
const app =  express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const nconf = require('nconf');

/*Config*/
const localConfigFile = 'config_local.json';
if (! fs.existsSync(localConfigFile)) {
    let defaultConfig = require('./config_defaults.json');
    fs.writeFileSync(localConfigFile, JSON.stringify(defaultConfig, null, 2));
}

nconf.argv().env().file({ file: localConfigFile});

/*Webserver*/
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/styles.css', express.static(__dirname + '/styles.css'));

/*Serial*/
const serialPortName = nconf.get('serial:serialPortPath');
const serialPort = new SerialPort(serialPortName, {baudRate: 19200});
const ByteLength = require('@serialport/parser-byte-length')
const parser = serialPort.pipe(new ByteLength({length: 1}))
parser.on('data', (data) => {
    console.log(data);
});

function serialTX(data) {
    if (! Array.isArray(data)) throw "serialTX: expected array";

    var b = new Buffer.from(data);
    serialPort.write(b);
    console.log(b);
}


/*WS*/
io.on('connection', function (socket) {
    console.log("New socketio connection from " + socket.handshake.address);

    socket.on('enableSafe', function() {
        console.log("Enable Safe");
        serialTX([128, 130]); //Initialise, safe control mode.
    });

    socket.on('enableFull', function() {
        console.log("Enable Full");
        serialTX([128, 131]); //Initialise, full control mode.
    });

   socket.on('startClean', function() {
        console.log("Start Clean");
        serialTX([135]); //Start clean
    });

   socket.on('sideBrushOn', function() {
       console.log("Side Brush On");
       serialTX([138, 1]); //Motors, side brush on.
   });

   socket.on('sideBrushOff', function() {
       console.log("Side Brush Off");
       serialTX([138, 0]); //Motors, side brush off.
   });

   //Receives move messages from the browser.
   socket.on('move', function(a, b, c, d) {
       console.log("Move " + a + " " + b + " " + c + " " + d);
       serialTX([137, a, b, c, d]); //Move, velocity MSB, velocity LSB, radius MSB, radius LSB
   });

   socket.on('7seg', function(a, b, c, d) {
       console.log("7 seg" + a + " " + b + " " + c + " " + d);
       serialTX([164, a, b, c, d]); //7 seg display, char1, char2, char3, char4 (characters as ASCII codes)
   })
});

const httpListenPort = nconf.get('webserver:httpListenPort');
http.listen(httpListenPort, function () {
    console.log('listening on *:' + httpListenPort);
});
